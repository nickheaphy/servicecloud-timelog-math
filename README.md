# ServiceCloud Timelog Math

A quick and dirty client side fix for ServiceCloud requiring manual timelogs to be entered in seconds.

Adds a Javascript listener to the textbox where the timelog seconds are entered. This listener looks for the key characters `h`, `m` and `q` which are used to indicate that the numbers previously entered should be treated as **h**ours, **m**inutes or **q**uarter hours. So if you enter `1h` the number `1` will be interpreted as **h**ours and be converted to `3600` seconds (`1m` would become `60` seconds and `1q` would become `900` seconds (15min))

Bonus: It also automatically populates the User field in the timelog based on the username entered into the `contentScript.js`

Short video of using here: [YouTube](https://youtu.be/p8o1jlbCbew)

## Setup and Installation

This is a Chrome browser userscript/extension, so you need to be using Chrome (you might be able to use something like [Tampermonkey](https://www.tampermonkey.net) to run this script inside any browser, but as I use Chrome I have only tested using Chrome)

1. Download the [repository](https://bitbucket.org/nickheaphy/servicecloud-timelog-math/get/master.zip) and extract `manifest.json`, `contentScript.json`, `calculator-16.png` and `calculator-128.png` and put them somewhere that you are not going to delete them (eg `Documents/ChromeScript/ServiceCloudTimelog`). It does not matter too much about the location, but Chrome is going to look in this folder every time you access ServiceCloud so don't put somewhere you will delete it.

2. Open the `contentScript.json` and edit the `sc_username` variable to reflect you own username (this is the username that you would normally have to enter into the timelog in the User textbox). Edit the `sc_case_group_default` and `sc_case_status_default` to set the default values for the Group and Status when creating a new case (note that the values for these are the underlying option values, not the text that is displayed onscreen. You may need to open the raw HTML of the page to discover the correct value for these fields). If you want to change any of the default comments, you can edit the `preset_comments` array and add anything you want.

3. Open Chrome and enter `chrome://extensions` into the URL.

4. Switch the "Developer Mode" on (switch at top right of page)

5. Select the "Load Unpacked" button and browse to the folder that contains the `manifest.json` and `contentScript.json` files.

Reload ServiceCloud and in the future subtract a few seconds off every timelog as you won't need to use a calculator to convert time to seconds.

### Updating to a new version

To update just copy the new version over the top of the existing version and then refresh the extension. To refresh, open Chrome and enter `chrome://extensions` into the URL, then click the refresh arrow. You should see the version increment.

## Improvements

There are probably lots of improvements that could be made to this code (eg it could have a GUI to allow you to enter the username, or better yet just store the data into a cookie), but I hope that in the future this is just fixed in the ServiceCloud code. IMHO it should intepret anything that looks something like time data and try and make logical sense of it. I want to enter "1h 15m 30s" or "1h15m30s" or "1hr 15.5min" or "15m30s1h" and not have it complain.

## Version

v1.0 - Initial version

v1.1 - Added the ability to set the defaults for the Group and Status of new cases.

v1.2 - Fire the change event when setting the default group to enable other interface elements.

v1.3 - Changed the default IDs of the fields with the change to Salesforce.

v1.4 - Changed an additional ID that I forgot and added some icons.

v1.5 - Added some Instructions for what should be entered into the 'Comments' for reporting purposes.

v1.6 - Fixed potential problem of instructions appearing on wrong pages

v1.7 - Added a dropdown with preset comments.

v1.8 - Fixed problem if caps lock was on.