// Setup variables
let sc_username = "Nick Heaphy"
let sc_new_record_type = "0122j000000kE0U"

// these two variables hold the html id's of the
// fields that we are looking for on the ServiceCloud pages
let sc_timelog_id = '00N0o00000NEfYk'
let sc_username_id = "CF00N0o00000NEfYh"
let sc_comment = "00N0o00000NEfYi"
let sc_record_type = "p3"

let sc_case_reason = "cas6"
let sc_case_reason_default = "Proprint NZ"

let sc_case_cat = "00N2000000ALPCz"

let sc_case_subcat = "00N2000000ALPD0"
let sc_case_subcat_default = "Other"

// What preset comments would you like
let preset_comments = ['Training', 'Travel to/from customer', 'PD:', 'Prep:'] 

// comment instructions
let comment_instructions = `<em>Travel:</em> Should have 'Travel' as the first word in the comment<br />
    <em>Training:</em> Should have 'Training' as the first word in the comment<br />
    <em>Personal Development:</em> Should have 'PD:' as the first word in the comment, or 'Learn' in the comment<br />
    `;

// this function called when items in the textbox change
// look for the magic characters and do the required math
// and put the new number in seconds back into the textbox
var performMath_nh = function(evt) {
    
    // get textbox content
    let lastchar = this.value[this.value.length-1]
    let thenumbers = this.value.slice(0, -1)

    switch (lastchar.toLowerCase()) {
        case 'h':
            this.value = Math.round(parseFloat(thenumbers) * 60 * 60)
            break
        case 'm':
            this.value = Math.round(parseFloat(thenumbers) * 60)
            break
        case 'q':
            this.value = Math.round(parseFloat(thenumbers) * 15 * 60)
            break
        default:
            // if the last character is not a number (or dot), and we have not already
            // matched it, might as well remove it (SF would report an error with it)
            if (lastchar != "." && isNaN(parseInt(lastchar, 10))) {
                this.value = thenumbers
            }
    }
};

// code to copy the selection text to the comment
var copyComment_nh = function(evt) {
    
    let selected_text = this.value
    var comment_textbox = document.getElementById(sc_comment)
    if (comment_textbox != null) {
        if (selected_text.length > 0) {
            if (comment_textbox.value.length > 0) {
                comment_textbox.value = selected_text + " " + comment_textbox.value
            } else {
                comment_textbox.value = selected_text
            }
        }
    }
};


// See if I can find the two timelog textboxes on the page
var timeloginput_nh = document.getElementById(sc_timelog_id)
var usernameinput_nh = document.getElementById(sc_username_id)

// if they are found, then for the timelog register the listener
// and for the username put in the username from the top of the script
if (timeloginput_nh != null) {

    // See if I can find the pbBody DIV and add some text to the end
    // need to do this before adding the listener
    var table_nh = document.getElementsByClassName("detailList")
    if (table_nh != null) {
        // build a comment preset dropdown
        var select = document.createElement('select')
        select.id = "preset_comments"
        select.style = "margin-top: 10px;"
        var option = document.createElement("option");
        select.appendChild(option);
        for (i = 0; i < preset_comments.length; ++i) {
            var option = document.createElement("option");
            option.value = preset_comments[i];
            option.text = preset_comments[i];
            select.appendChild(option);
        }
        select.addEventListener("change", copyComment_nh, false);

        var row = table_nh[0].insertRow(-1)
        var cell1 = row.insertCell(0)
        var cell2 = row.insertCell(1)
        cell2.colSpan = "3"

        cell1.innerHTML = "Instructions"
        cell1.className = "last labelCol"
        cell2.innerHTML = comment_instructions
        cell2.insertAdjacentElement("beforeend", select);
        cell2.className = "dataCol last col02"

        // inject
        //pbBodyDIV_nh[0].insertAdjacentHTML("beforeend", comment_instructions);
        //pbBodyDIV_nh[0].insertAdjacentElement("beforeend", select);
        
    }

    // attach the onchange to the time entry dialog
    timeloginput_nh.addEventListener('input', performMath_nh, false)

    if (usernameinput_nh != null) {
        usernameinput_nh.value = sc_username
    }
    console.log("Success: Found "+sc_timelog_id+" and setup with listener")
} else {
    console.log("Fail: Could not find element "+sc_timelog_id)
}



// See if I can find the 'Case Reason'
var casereason_nh = document.getElementById(sc_case_reason)
if (casereason_nh != null) {
    casereason_nh.value = sc_case_reason_default
    casereason_nh.dispatchEvent(new Event('change', { 'bubbles': true }))
}

var modifyCat = function(evt) {
    var subcat_nh = document.getElementById(sc_case_subcat)
    if (subcat_nh != null && subcat_nh.selectedIndex == 0) {
        console.log("Changing from "+subcat_nh.value+" to "+sc_case_subcat_default)
        subcat_nh.disabled = false
        subcat_nh.value = sc_case_subcat_default
    }
}

// attach a listener on case category to automatically select other as the default
var casecat_nh = document.getElementById(sc_case_cat)
if (casecat_nh != null) {
    console.log("Attaching event listener to "+sc_case_cat)
    casecat_nh.addEventListener('change', modifyCat)
}





// See if I can find the Case Record Type
var caserecordtype_nh = document.getElementById(sc_record_type)
if (caserecordtype_nh != null) {
    caserecordtype_nh.value = sc_new_record_type
}